﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageStatsPanel : MonoBehaviour
{
    public void UpdateData()
    {
        transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = World.WO.wood.ToString();
        transform.GetChild(1).transform.GetChild(1).GetComponent<Text>().text = World.WO.water.ToString();
        transform.GetChild(2).transform.GetChild(1).GetComponent<Text>().text = World.WO.food.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateData();
    }
}
