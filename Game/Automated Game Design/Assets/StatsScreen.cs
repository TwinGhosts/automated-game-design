﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental;
using UnityEngine;
using UnityEngine.UI;

public class StatsScreen : MonoBehaviour
{
    private List<String> valuesList;

    // Start is called before the first frame update
    void Start()
    {
        valuesList = new List<string>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetData(String name, int age, Human.Gender gender, Human.SexualOrientation sexualOrientation, int strength, int dexterity, int intelligence, int charisma)
    {
        if (valuesList != null)
        {
            valuesList.Clear();
            valuesList.Add(name);
            valuesList.Add(age.ToString());
            valuesList.Add(gender.ToString());
            valuesList.Add(sexualOrientation.ToString());
            valuesList.Add(strength.ToString());
            valuesList.Add(dexterity.ToString());
            valuesList.Add(intelligence.ToString());
            valuesList.Add(charisma.ToString());
        }

        UpdateStatScreen();
    }

    public void UpdateStatScreen()
    {
        for (int i = 0; i < valuesList.Count; i++)
        {
            transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text = valuesList[i];
        }
    }
}
