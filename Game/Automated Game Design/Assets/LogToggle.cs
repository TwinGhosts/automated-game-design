﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AClockworkBerry;

public class LogToggle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown("l"))
        {
            ScreenLogger.instance.ShowLog = !ScreenLogger.instance.ShowLog;
        }
    }
}
