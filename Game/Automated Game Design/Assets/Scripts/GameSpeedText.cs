﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSpeedText : MonoBehaviour
{
    private Text percentageText;
    // Start is called before the first frame update
    void Start()
    {
        percentageText = GetComponent<Text>();
    }

    // Update is called once per frame
    public void UpdateText(float value)
    {
        percentageText.text = "Gamespeed: " + value.ToString();
    }
}
