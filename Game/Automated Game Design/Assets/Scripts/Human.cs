﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    //Information
    [Header("Basic Info")]
    [SerializeField]
    string humanName;
    [SerializeField]
    public int age;
    [SerializeField]
    //the amount of generations this person had before him.
    public int generation = 0;
    [SerializeField]
    public Gender gender;

    public int averageChildrenAge = 35;

    [SerializeField]
    public SexualOrientation sexualOrientation;

    [Header("Looks")]
    public Material[] possibleLooks;
    private HumanBehaviour behaviour;

    //Stats
    [Header("Stats")]
    [SerializeField]
    public int strength;
    [SerializeField]
    public int dexterity;
    [SerializeField]
    public int intelligence;
    [SerializeField]
    public int charisma;

    //Survival Stats
    [Header("Survival Stats")]
    [SerializeField]
    public int maxHealth;
    [SerializeField]
    public int health;
    [SerializeField]
    public int maxFood;
    [SerializeField]
    int food;
    [SerializeField]
    public int maxWater;
    [SerializeField]
    int water;
    [SerializeField]
    int baseResourceDepletionRate = 5;
    [SerializeField]
    int maxSnowstormDepletion = 10;
    [SerializeField]
    int maxHomelessDepletion = 10;
    [SerializeField]
    public Human lifePartner;

    [Header("Parents")]
    public Human parentOne;
    public Human parentTwo;

    public House home;


    //Influences the offset of when they can have kids
    public int childrenAgeModifier;
    bool canHaveKids;
    public int amountOfChildren = 0;
    public int amountOfChildrenModifier;

    public int ageModifier;

    enum Priority { food, water, building, children, idle }

    public enum Gender { male, female, random }

    public enum SexualOrientation { hetero, gay, bi }

    [SerializeField]
    public HumanAction currentTask;

    private void Awake ()
    {
        behaviour = GetComponent<HumanBehaviour>();
    }

    // Start is called before the first frame update
    void Start ()
    {
        // Randomize look (not based on parents)
        Material look = (!parentOne && !parentTwo) ? possibleLooks[Random.Range(0, possibleLooks.Length)]
            : (Random.Range(0f, 1f) >= 0.5f) ? parentOne.GetComponentInChildren<MeshRenderer>().material
            : parentTwo.GetComponentInChildren<MeshRenderer>().material;

        GetComponentInChildren<MeshRenderer>().material = look;

        humanName = gameObject.name;
        food = maxFood;
        health = maxHealth;
        water = maxWater;
        //set age
        lifePartner = null;

        behaviour.AddAction(currentTask);
    }

    /// <summary>
    /// The reaction to the World.cs notify method.
    /// </summary>
    /// <param name="notification"></param>
    /// <param name="index"></param>
    public void Notify (World.HumanNotify notification, int index)
    {
        switch (notification)
        {
            case World.HumanNotify.increaseAge:

                Age(index);
                DetermineAction();
                break;
        }
    }

    /// <summary>
    /// Everything that happens to a human when a year has passed
    /// </summary>
    /// <param name="index"></param>
    void Age (int index)
    {
        age++;

        int snowstormModifier = 0;
        int homelessModifier = 0;
        if (home == null)
        {
            homelessModifier = Random.Range(0, maxHomelessDepletion);
        }
        if (World.WO.snowStorm)
        {
            snowstormModifier = Random.Range(0, maxSnowstormDepletion);
        }

        if (World.WO.food <= 0)
        {
            int breakpoint;
            breakpoint = 0;
            breakpoint++;
        }
        int restFood = 0;
        int restWater = 0;

        World.WO.food -= (Random.Range(1, baseResourceDepletionRate + homelessModifier + snowstormModifier));
        World.WO.water -= (Random.Range(1, baseResourceDepletionRate + homelessModifier + snowstormModifier));

        int maxResource = 0;
        //Fill personal pockets.
        if (food < maxFood && World.WO.food > 0)
        {
            World.WO.food -= (maxFood - food);
            food = maxFood;
        }

        if (water < maxWater && World.WO.water > 0)
        {
            World.WO.water -= (maxFood - maxWater);
            water = maxWater;
        }


        if (World.WO.food < 0)
        {
            restFood = Mathf.Abs(World.WO.food);
            World.WO.food = 0;
        }

        if (World.WO.water < 0)
        {
            restWater = Mathf.Abs(World.WO.water);
            World.WO.water = 0;
        }

        food -= restFood;
        water -= restWater;

        //if out of food or out of water, slowly lose health based on the amount of water or food you lack
        if (food < 0) { health += food; }
        if (water < 0) { water += food; }

        //Clamp all important values
        water = Mathf.Clamp(water, 0, maxWater);
        food = Mathf.Clamp(food, 0, maxFood);
        health = Mathf.Clamp(health, 0, maxHealth);

        //Die
        if (age > World.WO.averageAge + ageModifier - homelessModifier || health <= 0)
        {
            if (water <= 0)
            {
                Die(index, "dehydration");
            }
            else
            if (food <= 0)
            {
                Die(index, "starvation");
            }
            else
            if (age > World.WO.averageAge + ageModifier - homelessModifier)
            {
                Die(index, "old age");
            }
            else
            {
                Die(index, "unfortunate accident");
            }

        }

    }

    /// <summary>
    /// Kills the human. 
    /// </summary>
    /// <param name="index">Index number of this human in the World humans list</param>
    void Die (int index, string cause)
    {
        Debug.Log(this.gameObject.name + " passed away. Cause: " + cause + ". They will be missed.");
        World.WO.removeHuman(index);
        if (home != null)
            home.removeInhabitant(this);

        if (World.WO.humans.Count <= 0)
        {
            Debug.Log("This was the end of humanity. You lasted " + generation.ToString() + " generations");
        }
        World.WO.passedAway.Add(this);
        this.gameObject.SetActive(false);

    }

    /// <summary>
    /// The humans determine what task to perform based on how smart they are and on what their priorities are
    /// </summary>
    void DetermineAction ()
    {
        //See if the human can have a child yet
        canHaveKids = (age >= (World.WO.minChildrenAge + childrenAgeModifier) && age <= (World.WO.maxChildrenAge + childrenAgeModifier));

        //Determine what action you want to execute.
        Priority priority = DeterminePriority();

        //Then decide what task to perform
        currentTask = determineTask(priority);

        // Pass the task on the behaviour
        behaviour.AddAction(currentTask);
    }

    HumanAction determineTask (Priority priority)
    {
        switch (priority)
        {
            case Priority.food:
                if (World.WO.DiceRoll(20, intelligence, 10 - dexterity, StatType.Dexterity)) return HumanAction.fish;
                return HumanAction.farm;

            case Priority.water:
                if (World.WO.DiceRoll(40, intelligence + intelligence, 10, StatType.Intelligence)) return HumanAction.getRiverWater;
                return HumanAction.getSaltyWater;

            //Todo: Make it so he only woodchops when not having enough wood to build a house.
            case Priority.building:
                if (World.WO.DiceRoll(20, intelligence, 10 - strength, StatType.Strength) && World.WO.trees.Count > 0) return HumanAction.cutTrees;
                return HumanAction.idle;

            case Priority.children:
                if (World.WO.DiceRoll(20, charisma, 5, StatType.Intelligence))
                {
                    World.WO.lovers.Add(this);
                }
                return HumanAction.makeChild;

            default:
            case Priority.idle:
                return HumanAction.idle;
        }
    }

    /// <summary>
    /// The humans pick here which basic need they need to fullfill.
    /// </summary>
    /// <param name="smartMove"></param>
    /// <returns></returns>
    Priority DeterminePriority ()
    {
        List<float> percentages = new List<float>();
        List<Priority> priorities = new List<Priority>();

        int homelessModifier = 0, snowstormModifier = 0;

        if (home == null)
        {
            homelessModifier = maxHomelessDepletion;
        }
        if (World.WO.snowStorm)
        {
            snowstormModifier = maxSnowstormDepletion;
        }

        bool thoughtfullChoice = World.WO.DiceRoll(20, intelligence, 3, StatType.Intelligence);

        int resourceNeededPerPerson = baseResourceDepletionRate + homelessModifier + snowstormModifier;
        int totalResourceDepletionPerYear = resourceNeededPerPerson * World.WO.getAmountOfHouses();

        //If intelligence check succeeded
        if (thoughtfullChoice)
        {
            priorities.Add(Priority.food);
            percentages.Add(totalResourceDepletionPerYear / (World.WO.food + 1f));

            priorities.Add(Priority.water);
            percentages.Add(totalResourceDepletionPerYear / (World.WO.water + 1f));

            priorities.Add(Priority.building);
            percentages.Add(World.WO.getAmountOfHouses() / ((World.WO.getAmountOfHumans() / World.WO.humansPerHouse) + 1f)); //Building X van de 1

            //more desperate for children in their 20s till mid 30s
            if (amountOfChildren < World.WO.AverageChildrenPerCouple + amountOfChildrenModifier && canHaveKids)
            {
                float chanceOnGettingChild = World.WO.averageAge / (Mathf.Abs(age - averageChildrenAge) + 1f);
                priorities.Add(Priority.children);
                percentages.Add(chanceOnGettingChild); //Health X van de 1 
            }
        }
        else
        {
            priorities.Add(Priority.food);
            percentages.Add(4f);

            priorities.Add(Priority.water);
            percentages.Add(4f);

            priorities.Add(Priority.idle);
            percentages.Add(1f);

            priorities.Add(Priority.building);
            percentages.Add(4f);

            //more desperate for children in their 20s till mid 30s
            if (amountOfChildren < World.WO.AverageChildrenPerCouple + amountOfChildrenModifier && canHaveKids)
            {
                priorities.Add(Priority.children);
                percentages.Add(8f);
            }
        }

        float allStatsCombined = 0;

        foreach (float percentage in percentages)
        {
            allStatsCombined += percentage;
        }

        //The close the human rolls to the twenty, the higher the chance of choosing the best choice.
        float actionValue = Mathf.Clamp(Random.Range(0, allStatsCombined) + World.WO.DiceRollDifference(20, intelligence, 20), 0, allStatsCombined);
        float minimum = 0;
        float maximum = 0;
        for (int i = 0; i < percentages.Count; i++)
        {
            maximum = minimum + percentages[i];

            if (actionValue < maximum && actionValue >= minimum)
            {
                //The action has been found
                return priorities[i];
            }

            minimum = maximum;
        }

        return Priority.idle;
    }

    /// <summary>
    /// Sets What other person this human prefers to have children with
    /// </summary>
    /// <param name="partner"></param>
    public void setlifePartner (Human partner)
    {
        lifePartner = partner;
    }

    /// <summary>
    /// Gets what other person this human prefers to have children with
    /// </summary>
    /// <param name="partner"></param>
    public Human getlifePartner ()
    {
        return lifePartner;
    }

    public IEnumerator OnMouseDown ()
    {
        World.WO.StorageStatScreen.SetActive(false);
        World.WO.humanStatScreen.SetActive(true);
        yield return new WaitUntil(() => World.WO.humanStatScreen.activeInHierarchy == true);
        World.WO.humanStatScreen.GetComponent<StatsScreen>().GetData(humanName, age, gender, sexualOrientation, strength, dexterity, intelligence, charisma);
    }
}

//States for state machine
public enum HumanAction { idle, fish, farm, hunt, getSaltyWater, getRiverWater, cutTrees, makeChild };
