﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltWater : WaterBase
{
    public Fish fish = null;

    private void Start()
    {
        if (World.WO) World.WO.saltWaterTiles.Add(this);
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove()
    {
        if (World.WO && World.WO.saltWaterTiles.Contains(this)) World.WO.saltWaterTiles.Remove(this);
    }

    public void SpawnFish()
    {
        if (!fish)
        {
            var fishObject = Instantiate(World.WO.fishPrefab, transform.position, Quaternion.identity);
        }
    }

    private void OnDestroy()
    {
        Remove();
    }
}
