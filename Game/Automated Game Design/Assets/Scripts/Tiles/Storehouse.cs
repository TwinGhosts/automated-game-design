﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class Storehouse : MonoBehaviour
{

    private Color normalColor;
    private bool pickedUp = false;
    private Collider[] objectsCollidedWith;
    private Vector3 originalPosition;

    private void Awake()
    {
        normalColor = GetComponent<MeshRenderer>().material.color;
    }
    private void Start ()
    {
        if (World.WO) World.WO.storehouse = this;
    }

    private void Update ()
    {
        if (pickedUp)
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            this.transform.position = new Vector3(mousePosition.x, 0.5f, mousePosition.z);

            var checkedLayers = (1 << LayerMask.NameToLayer("Tree")) | (1 << LayerMask.NameToLayer("House")) | (1 << LayerMask.NameToLayer("Unbuildable"));
            objectsCollidedWith = Physics.OverlapBox(this.transform.position, new Vector3(this.GetComponent<NavMeshObstacle>().size.x, this.GetComponent<NavMeshObstacle>().size.z, this.GetComponent<NavMeshObstacle>().size.y), Quaternion.identity, checkedLayers);
            if (objectsCollidedWith.Length != 1)
            {
                this.GetComponent<MeshRenderer>().material.color = new Color(100, 0, 0);
                return;
            }
            this.GetComponent<MeshRenderer>().material.color = normalColor;
        }

        if (World.WO.gameStarted && pickedUp)
        {
            transform.position = originalPosition;
            pickedUp = false;
        }
    }

    public void OnMouseDown()
    {
        if (World.WO.gameStarted == false && !EventSystem.current.IsPointerOverGameObject())
        {
            originalPosition = transform.position;
        }
        else
        {
            World.WO.StorageStatScreen.SetActive(true);
            World.WO.humanStatScreen.SetActive(false);
        }
    }

    public void OnMouseDrag()
    {
        if (World.WO.gameStarted == false && !EventSystem.current.IsPointerOverGameObject())
        {
            pickedUp = true;
        }
    }

    public void OnMouseUp()
    {
        if (World.WO.gameStarted == false && pickedUp)
        {
            if (!EventSystem.current.IsPointerOverGameObject() && objectsCollidedWith.Length <= 1)
            {
                pickedUp = false;
            }
            else
            {
                transform.position = originalPosition;
                pickedUp = false;
                this.GetComponent<MeshRenderer>().material.color = normalColor;
            }
        }
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove ()
    {
        if (World.WO && World.WO.storehouse) World.WO.storehouse = null;
    }
}
