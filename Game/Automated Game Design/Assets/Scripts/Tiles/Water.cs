﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : WaterBase
{
    private void Start()
    {
        if (World.WO) World.WO.waterTiles.Add(this);
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove()
    {
        if (World.WO && World.WO.waterTiles.Contains(this)) World.WO.waterTiles.Remove(this);
    }

    public void OnDestroy()
    {
        Remove();
    }
}
