﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : MonoBehaviour
{
    public HumanBehaviour farmer = null;

    public Material mat_start;
    public Material mat_middle;
    public Material mat_end;

    public float currentTime = 0f;
    public float maxGrowTime = 9f;

    public bool Harvestable { get { return currentTime >= maxGrowTime; } }

    public float deathTimer = 8f;
    private float DEATH_TIMER;

    private void Start()
    {
        DEATH_TIMER = deathTimer;
        transform.SetParent(World.WO.farmParent);

        transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        transform.position = new Vector3(transform.position.x, 0.09f, transform.position.z);

        if (World.WO) World.WO.farms.Add(this);
    }

    private void Update()
    {
        // Grow behaviour
        if (farmer)
        {
            deathTimer = DEATH_TIMER;

            currentTime += Time.deltaTime;
            if (currentTime < maxGrowTime / 3f)
                SwapMaterial(mat_start);
            else if (currentTime < (maxGrowTime / 3f) * 2f)
                SwapMaterial(mat_middle);
            else if (currentTime < maxGrowTime)
                SwapMaterial(mat_end);
            else
                SwapMaterial(mat_end);

            currentTime = Mathf.Clamp(currentTime, 0f, maxGrowTime);
        }
        else
        {
            deathTimer -= Time.deltaTime;
            if (deathTimer < 0f)
            {
                Destroy(gameObject);
            }
        }
    }

    public void Harvest(float amount)
    {
        // Reset the growing
        currentTime = 0f;

        // Give the farmer resources
        if (farmer)
        {
            farmer.farmCurrentAmount += amount;
        }
    }

    /// <summary>
    /// Check to not keep swapping materials 24/7
    /// </summary>
    /// <param name="mat"></param>
    private void SwapMaterial(Material mat)
    {
        if (GetComponent<MeshRenderer>().material != mat)
            GetComponent<MeshRenderer>().material = mat;
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove()
    {
        if (World.WO && World.WO.farms.Contains(this)) World.WO.farms.Remove(this);
    }

    public void OnDestroy()
    {
        Remove();
    }
}
