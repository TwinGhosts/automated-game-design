using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class Tree : MonoBehaviour
{
    [Header("Materials")]
    public Material treeNormal;
    public Material treeCut;

    [Header("Specific Tree Vars")]
    public int woodAmount = 10;
    public float cutTime = 2.5f;
    public float decayTime = 2f;
    public float growTime = 10f; // In game years
    [HideInInspector] public bool isTargeted = false;
    [HideInInspector] public HumanBehaviour cutter = null;
    [HideInInspector] public bool isBeingCut = false;
    [HideInInspector] public bool isCut = false;
    [HideInInspector] public bool isFullyGrown = true;
    [HideInInspector] public Vector2 targetScale;

    private bool cursorIsOnObject = false;
    private float currentCutTime;
    public bool isPlaced = true;
    public bool isCursor = false;
    private Color normalColor;
    private Collider[] objectsCollidedWith;

    private Vector3 originalPosition;

    private void Awake ()
    {
        currentCutTime = cutTime;
        normalColor = GetComponent<MeshRenderer>().material.color;
        // Randomly scale the tree
        var scale = Random.Range(0.8f, 1.2f);
        targetScale = new Vector2(scale, scale);
    }

    private IEnumerator Start ()
    {
        if (World.WO) World.WO.trees.Add(this);

        transform.SetParent(World.WO.treeParent);

        // Grow the tree in world years, if it hasn't fully grown yet
        if (!isFullyGrown)
        {
            var progress = 0f;
            var startScale = Vector3.zero;
            var endScale = new Vector3(targetScale.x, targetScale.y, 1f);

            // Grow animation
            while (progress < 1f * World.WO.timePerYear)
            {
                progress += Time.deltaTime / growTime;
                transform.localScale = Vector3.Lerp(startScale, endScale, Mathf.SmoothStep(0f, 1f, progress));
                yield return null;
            }

            isFullyGrown = true;
        }
    }

    private void Update ()
    {
        if (Input.GetMouseButtonDown(1) && cursorIsOnObject)
        {
            Destroy(gameObject);
        }

        // When the tree is not placed yet it follows the mouse and changes the color if it is implacable
        if (!isPlaced)
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            this.transform.position = new Vector3(mousePosition.x, 0.5f, mousePosition.z);

            var checkedLayers = (1 << LayerMask.NameToLayer("Tree")) | (1 << LayerMask.NameToLayer("House")) | (1 << LayerMask.NameToLayer("Unbuildable"));
            objectsCollidedWith = Physics.OverlapBox(this.transform.position, new Vector3(this.GetComponent<NavMeshObstacle>().radius, this.GetComponent<NavMeshObstacle>().radius, this.GetComponent<NavMeshObstacle>().radius), Quaternion.identity, checkedLayers);
            if (objectsCollidedWith.Length <= 1 && !EventSystem.current.IsPointerOverGameObject())
            {
                this.GetComponent<MeshRenderer>().material.color = normalColor;
            }
            else
                this.GetComponent<MeshRenderer>().material.color = new Color(100, 0, 0);

        }
        // When the tree is being cut, lower the 'life' of the tree
        if (isBeingCut && !isCut) currentCutTime -= Time.deltaTime;

        // When the 'life' of a tree has been depleted, make it cut
        if (currentCutTime <= 0f && !isCut)
        {
            isCut = true;
            GetComponent<MeshRenderer>().material = treeCut;
            if (cutter)
                cutter.carriedWood += World.WO.DiceRollDifference(woodAmount, cutter.GetHuman().strength, 0); // Give the cutter the wood

            // Have the chance to plant 0 to 3 trees
            for (int i = 0; i < cutter.maxTreePlantAmount; i++)
            {
                if (World.WO.DiceRoll(20, cutter.GetHuman().intelligence, 15, StatType.Intelligence, cutter.GetHuman()))
                {
                    BuildUtility.PlantTree(gameObject, 1);
                }
            }
        }

        // When the tree is cut, start removing it from the map after a specific amount of time
        if (isCut) decayTime -= Time.deltaTime;

        // Remove the tree when timer has depleted
        if (isCut && decayTime <= 0f)
        {
            Remove();
            Destroy(gameObject);
        }

        // Unit must keep cutting the tree to make it be cut, otherwise it resets
        isBeingCut = false;
    }

    public void OnMouseEnter ()
    {
        cursorIsOnObject = true;
    }

    public void OnMouseExit ()
    {
        cursorIsOnObject = false;
    }

    public void OnMouseDown ()
    {
        if (World.WO.gameStarted == false && !EventSystem.current.IsPointerOverGameObject())
        {
            originalPosition = transform.position;
        }
    }

    /// <summary>
    /// Places a tree
    /// </summary>
    public void OnMouseDrag ()
    {
        if (World.WO.gameStarted == false && !EventSystem.current.IsPointerOverGameObject())
        {
            isPlaced = false;
        }
    }

    public void OnMouseUp ()
    {
        if (World.WO.gameStarted == false)
        {
            if (!isPlaced)
            {
                if (objectsCollidedWith.Length <= 1 && !EventSystem.current.IsPointerOverGameObject())
                {
                    if (isCursor)
                    {
                        //isPlaced = true;
                        GameObject newTree = Instantiate(gameObject, transform.position, transform.rotation);
                        newTree.GetComponent<Tree>().isPlaced = true;
                        newTree.GetComponent<Tree>().isCursor = false;
                    }
                    else isPlaced = true;
                }
                else
                {
                    if (!isCursor)
                    {
                        transform.position = originalPosition;
                        isPlaced = true;
                        this.GetComponent<MeshRenderer>().material.color = normalColor;
                    }
                }

            }
        }
    }

    public void Cut ()
    {
        isBeingCut = true;
    }

    private void OnDestroy ()
    {
        Remove();
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove ()
    {
        if (World.WO && World.WO.trees.Contains(this)) World.WO.trees.Remove(this);
    }
}
