﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    [HideInInspector] public float amountOfFish;
    public HumanBehaviour fisher = null;

    private void Start()
    {
        transform.SetParent(World.WO.fishParent);
        amountOfFish = Random.Range(25f, 150f);
        if (World.WO) World.WO.fish.Add(this);
        transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        transform.position += new Vector3(0f, 0.01f, 0f);
    }

    private void Update()
    {
        if (amountOfFish <= 0f)
            Remove();
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove()
    {
        if (World.WO && World.WO.fish.Contains(this))
        {
            World.WO.fish.Remove(this);
            Destroy(gameObject);
        }
    }
}
