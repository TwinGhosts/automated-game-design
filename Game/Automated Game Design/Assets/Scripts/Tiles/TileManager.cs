﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileManager : MonoBehaviour
{
    public TileType activeType;
    public Material activeMaterial;
    private GameObject activeGameObject;

    public Material grassMaterial;
    public Material waterMaterial;
    public Material mountainMaterial;
    public Material snowMaterial;

    public GameObject grassTile;
    public GameObject waterTile;
    public GameObject saltWaterTile;
    public GameObject mountainTile;

    private GameObject t;
    public GameObject tree;

    public void Start ()
    {
        Time.timeScale = 1f;
    }

    private void Awake()
    {
        activeMaterial = grassMaterial;
    }

    public Material GetActiveTileMaterial ()
    {
        return activeMaterial;
    }

    public TileType GetActiveType ()
    {
        return activeType;
    }

    public GameObject GetActiveGameObject ()
    {
        return activeGameObject;
    }

    public void ChangeTileType (String type)
    {

        switch (type)
        {
            case "Grass":
                if (this.t != null) Destroy(t);
                Debug.Log("Change to Grass");
                activeMaterial = grassMaterial;
                activeType = TileType.Grass;
                activeGameObject = grassTile;
                break;
            case "Water":
                if (this.t != null) Destroy(t);
                Debug.Log("Change to Water");
                activeMaterial = waterMaterial;
                activeType = TileType.Water;
                activeGameObject = waterTile;
                break;
            case "Mountain":
                if (this.t != null) Destroy(t);
                Debug.Log("Change to Mountain");
                activeMaterial = mountainMaterial;
                activeType = TileType.Mountain;
                activeGameObject = mountainTile;
                break;
            case "Snow":
                if (this.t != null) Destroy(t);
                Debug.Log("Change to Snow");
                activeMaterial = snowMaterial;
                activeType = TileType.Snow;
                activeGameObject = grassTile;
                break;
            case "Tree":
                if (this.t != null) Destroy(t);
                Vector3 mousePosition = Input.mousePosition;
                mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
                t = Instantiate(tree);
                t.transform.position = new Vector3(mousePosition.x, 0.5f, mousePosition.z);
                t.GetComponent<Tree>().isPlaced = false;
                t.GetComponent<Tree>().isCursor = true;
                break;
            case "SaltWater":
                if (this.t != null) Destroy(t);
                activeType = TileType.SaltWater;
                activeMaterial = saltWaterTile.GetComponent<MeshRenderer>().sharedMaterial;
                activeGameObject = saltWaterTile;
                break;
            case "None":
                if (this.t != null) Destroy(t);
                break;
            default:
                if (this.t != null) Destroy(t);
                activeMaterial = grassMaterial;
                break;


        }
    }
}