﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BuildUtility
{
    /// <summary>
    /// Gets the new position.
    /// </summary>
    /// <returns>The new position.</returns>
    public static Vector3 GetNewBuildPosition(float buildRadius, int currentRecursion, int maxRecursion, GameObject checkedObject, float radius, bool more = false)
    {
        var checkedLayers = (1 << LayerMask.NameToLayer("Tree")) | (1 << LayerMask.NameToLayer("House")) | (1 << LayerMask.NameToLayer("Unbuildable"));
        Vector3 errorVector = Vector3.one * 9999f; // This is not gucci
        Vector3 targetPosition = checkedObject.transform.position + Random.insideUnitSphere * buildRadius;
        targetPosition.y = 0f;

        // Break out of the recursion after some time
        if (currentRecursion > maxRecursion)
        {
            Debug.LogError("ERROR > Could not find a valid position to place a house!");
            return errorVector;
        }

        // Recurse untill a position has been found
        Collider[] objectsCollidedWith = Physics.OverlapBox(targetPosition, new Vector3(radius, radius, radius), Quaternion.identity, checkedLayers);

        // Increase the buildradius size when not finding a location
        if (currentRecursion > maxRecursion)
        {
            buildRadius += 1f;
        }

        // When the spot is taken, try again somewhere else
        if (objectsCollidedWith.Length > 0)
        {
            return GetNewBuildPosition(buildRadius, currentRecursion + 1, maxRecursion, checkedObject, radius);
        }
        else if (more && objectsCollidedWith.Length == 1)
        {
            return targetPosition;
        }
        else
        { // when viable, take this spot
            return targetPosition;
        }
    }

    /// <summary>
    /// Plants new trees around the parentObject.
    /// The amount might differ if there isn't enough space to avoid too much recursion
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="amount"></param>
    public static void PlantTree(GameObject parentObject, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            var plantPosition = GetNewBuildPosition(2f, 0, 20, parentObject, 0.5f);
            plantPosition.y = 0.5f;
            var tree = Object.Instantiate(World.WO.treePrefab, plantPosition, Quaternion.identity);
            tree.isFullyGrown = false;
            tree.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
            World.WO.trees.Add(tree);
        }
    }
}
