﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanBehaviour : MonoBehaviour
{
    private Human human;
    private NavMeshAgent agent;

    [Header("Actions")]
    public HumanAction currentAction;
    public HumanAction previousAction;

    public Stack<HumanAction> actionStack = new Stack<HumanAction>();

    [Header("Tree Cutting")]
    #region Tree Cutting & Building
    public Tree targetedTree = null;
    public int carriedWood = 0;
    private static readonly int buildingWoodCost = 20;
    private Vector3? buildPosition = null;
    private House builtHouse = null;
    public int maxTreePlantAmount = 3;
    #endregion Tree Cutting

    [Header("Fishing")]
    #region Fishing
    public float fishMaxAmount = 100;
    public float fishCurrentAmount = 0;
    public float fishGatherInterval = 4f;
    public Fish fishTarget = null;
    #endregion Fishing

    [Header("Farming")]
    #region Farming
    public float farmMaxFoodAmount = 80;
    public float farmCurrentAmount = 0;
    public float farmGatherInterval = 3f;
    public Farm farmTarget = null;
    #endregion Farming

    [Header("Water Gathering")]
    #region Water Gathering
    public float waterMaxAmount = 1000;
    public float waterCurrentAmount = 0;
    public float waterGatherInterval = 2.5f;
    public WaterBase waterTarget = null;
    #endregion Water Gathering

    [Header("Idle")]
    #region Idle
    [HideInInspector] public Vector3 targetPatrolPosition;
    private Vector3 errorVector = Vector3.one * 9999f;
    #endregion Idle

    private bool isCollecting = false;

    private void Awake()
    {
        human = GetComponent<Human>();
        agent = GetComponent<NavMeshAgent>();
        targetPatrolPosition = errorVector;

        //Determine how much of everything can be carried
        waterMaxAmount *= 1.00f / (World.WO.DiceRollDifference(8, human.strength, 0));
        fishMaxAmount *= 1.00f / (World.WO.DiceRollDifference(8, human.strength, 0));
        farmMaxFoodAmount *= 1.00f / (World.WO.DiceRollDifference(8, human.intelligence, 0));
    }

    private void Start()
    {
        transform.SetParent(World.WO.humanParent);
    }

    private void Update()
    {
        // State machine
        switch (currentAction)
        {
            default:
            case HumanAction.idle:
                IdlePatrol();

                // Make sure to always stop the idle action when other actions are queued
                if (actionStack.Count > 0)
                {
                    FinishCurrentAction();
                }
                break;

            case HumanAction.fish:
                FishBehaviour();
                break;

            case HumanAction.farm:
                FarmBehaviour();
                break;

            case HumanAction.hunt:
                // Not implemented yet
                FinishCurrentAction();
                break;

            case HumanAction.getSaltyWater:
                GatherWater(true);
                break;

            case HumanAction.getRiverWater:
                GatherWater(false);
                break;

            case HumanAction.cutTrees:
                CutAndBuildBehaviour();
                break;

            case HumanAction.makeChild:
                FishBehaviour();
                break;
        }
    }

    #region Non Behaviour
    /// <summary>
    /// Sets a new action for the human to do.
    /// Can override all of the current actions or it can be placed at the end of the stack
    /// </summary>
    /// <param name="overrideCurrentActions"></param>
    public void AddAction(HumanAction action, bool overrideCurrentActions = false)
    {
        if (overrideCurrentActions) actionStack.Clear();
        actionStack.Push(action);

        // When this is the only action in the list, just activate it
        if (actionStack.Count == 1 && currentAction == HumanAction.idle)
        {
            FinishCurrentAction();
        }
    }

    /// <summary>
    /// Removes the current action and sets the previous and next actions
    /// </summary>
    public void FinishCurrentAction()
    {
        // Swap the previous action with the current action
        previousAction = currentAction;

        // Find a new action to set as the current one or use the idle one if none are present
        if (actionStack.Count > 0)
            currentAction = actionStack.Pop();
        else
            AddAction(HumanAction.idle);

        // Stop the general collecting value
        isCollecting = false;
    }

    /// <summary>
    /// Does what it says, goes through all trees and determines the closest available one to cut
    /// </summary>
    /// <returns></returns>
    private Tree FindClosestTreeFromUnit()
    {
        // Init vars
        var trees = World.WO.trees;
        var closestDistance = Mathf.Infinity;
        Tree closestTree = null;

        // Go through all trees
        foreach (var tree in trees)
        {
            // Tree mustn't be in use or unavailable and fully grown
            if (!tree.isTargeted && !tree.isCut && !tree.isBeingCut && tree.isFullyGrown)
            {
                // Check the distance, and if it's smaller than the current value, store it and the tree
                var distance = Vector3.Distance(transform.position, tree.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestTree = tree;
                }
            }
        }

        return closestTree;
    }

    /// <summary>
    /// Does what it says, goes through all fish tiles and determines the closest available one to harvest
    /// </summary>
    /// <returns></returns>
    private Fish FindClosestFishFromUnit()
    {
        // Init vars
        var fishTiles = World.WO.fish;
        var closestDistance = Mathf.Infinity;
        Fish closestFish = null;

        // Go through all fish tiles
        foreach (var fish in fishTiles)
        {
            // Fish mustn't be in use
            if (!fish.fisher)
            {
                // Check the distance, and if it's smaller than the current value, store it and the tree
                var distance = Vector3.Distance(transform.position, fish.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestFish = fish;
                }
            }
        }

        return closestFish;
    }

    /// <summary>
    /// Does what it says, goes through all water tiles and determines the closest available one
    /// </summary>
    /// <returns></returns>
    private WaterBase FindClosestWaterSourceToGatherer(bool salty)
    {
        // Init vars
        var saltWaterTiles = World.WO.saltWaterTiles;
        var waterTiles = World.WO.waterTiles;
        var closestDistance = Mathf.Infinity;
        WaterBase closestSource = null;

        // Go through all trees
        if (salty)
        {
            foreach (var saltWater in saltWaterTiles)
            {
                // Tree mustn't be in use or unavailable and fully grown
                if (!saltWater.gatherer)
                {
                    // Check the distance, and if it's smaller than the current value, store it and the tree
                    var distance = Vector3.Distance(transform.position, saltWater.transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestSource = saltWater;
                    }
                }
            }
        }
        else
        {
            foreach (var water in waterTiles)
            {
                // Tree mustn't be in use or unavailable and fully grown
                if (!water.gatherer)
                {
                    // Check the distance, and if it's smaller than the current value, store it and the tree
                    var distance = Vector3.Distance(transform.position, water.transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestSource = water;
                    }
                }
            }
        }

        return closestSource;
    }

    private Farm FindEmptyFarm()
    {
        // Loop through all farms
        foreach (var farm in World.WO.farms)
        {
            // Find one which doesn't have a farmer and return it
            if (!farm.farmer)
            {
                return farm;
            }
        }

        // None found which are empty
        return null;
    }
    #endregion Non Behaviour

    #region Behaviour
    /// <summary>
    /// All of the logic that is about finding a tree,
    /// cutting it, possibly planting them and delivering the resources.
    /// </summary>
    private void CutAndBuildBehaviour() // Also has the building behaviour
    {
        if (carriedWood < buildingWoodCost)
        {
            // Must have a tree to target when there isnt one and the human isnt carrying wood
            if (!builtHouse && (targetedTree == null || targetedTree.isCut) && carriedWood < buildingWoodCost)
            {
                targetedTree = FindClosestTreeFromUnit();
                targetedTree.isTargeted = true;
                targetedTree.cutter = this;
            }

            // When there is a target tree which isn't cut, go to it
            if (!builtHouse && targetedTree != null && carriedWood < buildingWoodCost)
            {
                // Check whether we are in range of the tree
                if (Vector3.Distance(transform.position, targetedTree.transform.position) < 2f)
                {
                    // Start cutting
                    targetedTree.Cut();
                    if (Random.Range(0f, 100f) > 90f) human.strength += 1;
                }
                else
                {
                    // Move to the tree if it isn't the destination yet
                    if (agent.destination != targetedTree.transform.position)
                    {
                        agent.SetDestination(targetedTree.transform.position);
                    }
                }
            }
        }
        else
        {
            // When the wood limit has been reached, go find a spot to build
            if (!builtHouse && carriedWood >= buildingWoodCost && buildPosition == null)
            {
                buildPosition = BuildUtility.GetNewBuildPosition(World.WO.settlementRadius, 0, 50, World.WO.storehouse.gameObject, House.houseSize.x, true);

                // When no position has been found, stop trying
                if (buildPosition == errorVector)
                {
                    carriedWood = 0;
                    builtHouse = null;
                    buildPosition = null;
                    targetedTree = null;
                    FinishCurrentAction();
                }
            }

            // When a spot has been found, build the house
            if (!builtHouse && carriedWood >= buildingWoodCost && buildPosition != null)
            {
                // Move to the build position first
                var tempBuildPos = (Vector3)buildPosition;
                tempBuildPos.y = 0.5f; // set at the correct height
                var distance = Vector3.Distance(transform.position, tempBuildPos);
                agent.SetDestination(tempBuildPos);

                // Check for the distance between the unit and the build position
                if (distance < 2f)
                {
                    //Increase chance on accidents when its snowing
                    float diceRollInfluencer = 0;
                    if (World.WO.snowStorm)
                    {
                        diceRollInfluencer = World.WO.snowStormResourceModifier;
                    }

                    builtHouse = Instantiate(World.WO.housePrefab, tempBuildPos, Quaternion.identity);
                    builtHouse.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
                    builtHouse.isBuilt = false;
                    builtHouse.Build();

                    if (World.WO.DiceRoll(20, human.dexterity + human.strength, (int)(8 * diceRollInfluencer), StatType.Strength))
                    {
                        int damage;
                        damage = World.WO.DiceRollDifference(20, human.strength, 20);
                        if (Random.Range(0f, 100f) > 70f) human.strength -= 1;
                        Debug.Log(human.gameObject.name + " hurt themselves in a horrible building accident. They lose " + damage.ToString() + "HP");
                        human.health -= damage;
                    }

                }
            }
        }

        // When the house is done, reset and finish the action
        if (builtHouse && builtHouse.isBuilt)
        {
            builtHouse = null;
            carriedWood = 0;
            targetedTree = null;
            buildPosition = null;
            FinishCurrentAction();
        }
    }

    private void FishBehaviour()
    {
        // If not enough fish yet
        if (fishCurrentAmount < fishMaxAmount)
        {
            // and no target
            if (!fishTarget)
            {
                isCollecting = false;

                // Find fish
                var source = FindClosestFishFromUnit();

                // Break out of the action when there are no fish tiles
                if (!source)
                {
                    FinishCurrentAction();
                    return;
                }

                fishTarget = source;
                agent.SetDestination(fishTarget.transform.position);
            }

            // When a targeted fish tile is found
            else
            {
                // When not fishing, check distance
                if (!isCollecting)
                {
                    // When the water tile is in range
                    var isInRangeOfFish = Vector3.Distance(transform.position, fishTarget.transform.position) < 2f;
                    if (isInRangeOfFish)
                    {
                        isCollecting = true;
                    }
                }
                else
                {
                    // Increase the current fish carried
                    fishCurrentAmount += (Time.deltaTime / fishGatherInterval) * fishMaxAmount;
                    fishTarget.amountOfFish -= (Time.deltaTime / fishGatherInterval) * fishMaxAmount;
                }
            }
        }
        // When enough fish have been gathered return them
        else
        {
            fishTarget = null;
            isCollecting = false;
            agent.SetDestination(World.WO.storehouse.transform.position);

            // Check distance to storehouse
            if (Vector3.Distance(transform.position, World.WO.storehouse.transform.position) < 1.5f)
            {
                if (Random.Range(0f, 100f) > 80f) human.dexterity += 1;
                World.WO.food += (int)((fishCurrentAmount + World.WO.DiceRollDifference(20, human.dexterity, 20) * 2) * World.WO.snowStormResourceModifier);
                fishCurrentAmount = 0f;
                FinishCurrentAction();
            }
        }
    }

    private void FarmBehaviour()
    {
        // Find or create a farm when none is targeted
        if (!farmTarget)
        {
            // Try to find an empty one
            var emptyFarm = FindEmptyFarm();

            // When found, start moving to it
            if (emptyFarm)
            {
                farmTarget = emptyFarm;
                farmTarget.farmer = this;
                agent.SetDestination(farmTarget.transform.position);
            }
            // Else when one can't be found, create one
            else
            {
                // Try to build a farm at an unoccupied location
                var buildPos = BuildUtility.GetNewBuildPosition(World.WO.settlementRadius, 0, 20, World.WO.storehouse.gameObject, 0.75f);
                farmTarget = Instantiate(World.WO.farmPrefab, new Vector3(buildPos.x, .4f, buildPos.z), Quaternion.identity);
                farmTarget.farmer = this;

                // Check if it succeeded to build a farm
                if (farmTarget)
                    agent.SetDestination(farmTarget.transform.position);
            }
        }
        // When a farm has been found
        else
        {
            // Check for the distance whent he unit isn't collecting yet
            if (farmCurrentAmount == 0)
            {
                var distanceCheck = Vector3.Distance(transform.position, farmTarget.transform.position) < 1.25f;

                // Check for the farm to be harvestable
                if (distanceCheck && farmTarget.Harvestable)
                {
                    // Gather food
                    farmTarget.Harvest(farmMaxFoodAmount);
                }
            }
            // When the farm isn't harvestable and the unit has food, return it
            else
            {
                // Set the destination to the storehouse
                if (agent.destination != World.WO.storehouse.transform.position)
                {
                    agent.SetDestination(World.WO.storehouse.transform.position);
                }

                // Check for the distance to the storehouse when the unit has food
                var distanceCheck = Vector3.Distance(transform.position, World.WO.storehouse.transform.position) < 1.25f;
                if (distanceCheck)
                {
                    // Set the destination to the farm
                    if (agent.destination != farmTarget.transform.position)
                    {
                        //If snowstorm, do what they earned times half
                        if (World.WO.snowStorm) farmCurrentAmount *= World.WO.snowStormResourceModifier;
                        agent.SetDestination(farmTarget.transform.position);
                        World.WO.food += (int)farmCurrentAmount + World.WO.DiceRollDifference(20, human.intelligence, 20) * 2;
                        if (Random.Range(0f, 100f) > 85f) human.intelligence += 1;
                        farmCurrentAmount = 0;
                        isCollecting = false;
                        farmTarget.farmer = null;
                        FinishCurrentAction();
                    }
                }
            }
        }
    }

    private void GatherWater(bool salty)
    {
        // When the gatherer doesn't have the required amount of water yet
        if (waterCurrentAmount < waterMaxAmount)
        {
            // When there is no targeted tile yet, find one
            if (!waterTarget)
            {
                // Find a salt water tile
                var source = FindClosestWaterSourceToGatherer(salty);
                // Break out of the action when there are no salt water tiles
                if (!source)
                {
                    FinishCurrentAction();
                    return;
                }

                // Set the target to the found source, also move to it
                waterTarget = source;
                agent.SetDestination(source.transform.position);
            }
            // When a water tile has been found
            else
            {
                // When not collecting water, check distance
                if (!isCollecting)
                {
                    // When the water tile is in range
                    var isInRangeOfWater = Vector3.Distance(transform.position, waterTarget.transform.position) < 2f;
                    if (isInRangeOfWater)
                    {
                        isCollecting = true;
                    }
                }
                else
                {
                    // Increase the current water carried
                    waterCurrentAmount += (Time.deltaTime / waterGatherInterval) * waterMaxAmount;
                }
            }
        }
        // When enough water has been gathered, return it
        else
        {
            // When the storehouse isn't the destination yet, set it
            if (agent.destination != World.WO.storehouse.transform.position)
            {
                agent.SetDestination(World.WO.storehouse.transform.position);
            }

            // Check distance to storehouse and deliver when close
            if (Vector3.Distance(transform.position, agent.destination) < 2.5f)
            {
                if (World.WO.snowStorm)
                {
                    waterCurrentAmount *= World.WO.snowStormResourceModifier;
                }
                if (salty)
                {
                    World.WO.water -= (int)waterCurrentAmount + World.WO.DiceRollDifference(20, human.strength, 20) * 2;          // Deliver the water
                    int damage = World.WO.DiceRollDifference(20, human.intelligence, 20);
                    human.health -= damage;     // Lower health because of the salt
                    Debug.Log(human.gameObject.name + " hurt themselves trying to drink Salty water, losing " + damage.ToString() + "HP");
                }
                else
                {
                    World.WO.water += (int)waterCurrentAmount + World.WO.DiceRollDifference(20, human.strength, 20) * 2;          // Deliver the water
                }

                // Reset variables and the action
                waterCurrentAmount = 0;
                waterTarget = null;
                FinishCurrentAction();
            }
        }
    }

    /// <summary>
    /// Randomly patrols the village
    /// </summary>
    public void IdlePatrol()
    {
        // Choose patrol position when not chosen
        if (targetPatrolPosition == errorVector)
        {
            targetPatrolPosition = World.WO.storehouse.transform.position + Random.insideUnitSphere * World.WO.settlementRadius;
            agent.SetDestination(targetPatrolPosition);
        }
        // Set the destination to that position
        else
        {
            // Make the target pos 2D instead of 3D for better distance checking
            var nonDepthVector = transform.position;
            nonDepthVector.y = 0f;

            var nonDepthTarget = targetPatrolPosition;
            nonDepthTarget.y = 0f;

            // When close to target wait for a moment
            if (Vector3.Distance(nonDepthVector, nonDepthTarget) < 1f)
            {
                FinishCurrentAction();
            }
        }
    }
    #endregion Behaviour

    #region Get/Set
    public Human GetHuman()
    {
        return human;
    }
    #endregion Get/Set
}
