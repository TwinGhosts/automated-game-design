﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StatEvent
{
    public static bool StatCheck (float checkedStatValue, float upperBound, float modifiers = 0)
    {
        var randomRoll = Random.Range(Stat.generalStatMin, checkedStatValue + modifiers);
        Debug.Log("Rolled: " + randomRoll + " / " + upperBound);
        return (randomRoll >= upperBound);
    }
}
