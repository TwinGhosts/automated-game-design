﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lexic;
using UnityEngine.UI;
using UnityEngine.AI;

public class World : MonoBehaviour
{
    public static World WO;

    public bool gameStarted;

    public GameObject humanStatScreen;
    public GameObject StorageStatScreen;

    public bool snowStorm;
    public float snowStormResourceModifier = 0.5f;
    [SerializeField]
    int snowStormGeneration = 10;
    //This is done times the amount of resources;

    // Resources
    [Header("Resources")]
    public int wood = 1000;
    public int food = 1000;
    public int water = 1000;

    //World information
    [Header("World Information")]
    [SerializeField]
    int year = 0;
    [SerializeField]
    bool aging = true;

    // All buildings and resource pools
    [HideInInspector] public List<Tree> trees = new List<Tree>();
    [HideInInspector] public List<House> houses = new List<House>();
    [HideInInspector] public List<Farm> farms = new List<Farm>();
    [HideInInspector] public List<Water> waterTiles = new List<Water>();
    [HideInInspector] public List<SaltWater> saltWaterTiles = new List<SaltWater>();
    [HideInInspector] public List<Fish> fish = new List<Fish>();
    [HideInInspector] public Storehouse storehouse;

    //Time in seconds for a year to pass.
    public float timePerYear = 10f;
    float timeLeft = 0;

    //Information about the humans
    [Header("Human Information")]
    [SerializeField]
    int geneMutation = 3;

    [SerializeField]
    float chanceOnGenderModifier = 0.8f;

    [SerializeField]
    int maleMutation = 2;

    [SerializeField]
    int femaleMutation = 2;

    [SerializeField]
    int maxAmountOfHumans = 200;

    [SerializeField]
    int startAmountOfHumans = 16;

    [SerializeField]
    public int averageAge = 79;

    [SerializeField]
    float chanceToGetHetero = 0.9f;

    [SerializeField]
    float chanceToGetGay = 0.05f;

    [SerializeField]
    float chanceToGetBi = 0.05f;

    [SerializeField]
    float chanceToGetMale = 0.5f;

    //From what age you can get kids
    public int minChildrenAge = 14;

    //From what age you can no longer have children
    public int maxChildrenAge = 69;


    [SerializeField]
    public int AverageChildrenPerCouple = 4;

    [SerializeField]
    GameObject humanPrefab;

    //Keep track of all existing humans
    [HideInInspector] public List<Human> humans;
    [HideInInspector] public List<Human> passedAway;
    int currentGeneration = 0;
    int lastCleanedGeneration = 0;
    int distanceBetweenCleaning = 3;
    //Keep track of possible lovers
    public List<Human> lovers;

    public int humansPerHouse = 4;

    NameGenerator nameGen;
    //ENUM with all actions to notify the humans with
    public enum HumanNotify { increaseAge, endOfWorld };

    [Header("Settlement")]
    public float settlementRadius = 10f;
    public House housePrefab;
    public Tree treePrefab;
    public Farm farmPrefab;
    public GameObject terrainParent; // for the height of the units
    public Fish fishPrefab;

    [Header("Parent Objects")]
    public Transform treeParent;
    public Transform houseParent;
    public Transform humanParent;
    public Transform farmParent;
    public Transform fishParent;

    [Header("Snowstorm")]
    public Graphic snowStormOverlay;
    private ParticleSystem snowStormParticleSystem;

    private void Awake()
    {
        snowStormParticleSystem = Camera.main.GetComponentInChildren<ParticleSystem>();

        if (WO != null)
        {
            GameObject.Destroy(WO);
        }
        else
        {
            WO = this;
        }

        DontDestroyOnLoad(this);
        nameGen = GetComponent<NameGenerator>();
        humans = new List<Human>();
        passedAway = new List<Human>();
        houses = new List<House>();

        //Initializing variables.
        timeLeft = timePerYear;
    }

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => WO.gameStarted);
        Debug.Log("game started");
        House currentHouse = null;
        //Create first 4 humans
        for (int i = 0; i < startAmountOfHumans; i++)
        {
            if (i % humansPerHouse == 0)
            {
                currentHouse = createHouse();

            }
            int age;
            Human human;
            //Create a man
            if (i % 2 == 0)
            {
                age = Random.Range(20, 30);
                human = createHuman(Random.Range(-3, 4), Random.Range(-3, 4), Random.Range(-3, 4), Random.Range(-3, 4), 100, 100, 100, Human.Gender.male, 0, 0, 0, age);
            }
            else //Create a woman
            {
                age = Random.Range(20, 30);
                human = createHuman(Random.Range(-3, 4), Random.Range(-3, 4), Random.Range(-3, 4), Random.Range(-3, 4), 100, 100, 100, Human.Gender.female, 0, 0, 0, age);
            }

            AddHumanToHouse(human, currentHouse);

        }
    }
    public void startGame()
    {
        WO.gameStarted = true;
    }

    /// <summary>
    /// Adds a human to a house. and adds a reference to that house in the human.
    /// </summary>
    /// <param name="human">Person to inhabit the house</param>
    /// <param name="house">house to inhabit</param>
    private void AddHumanToHouse(Human human, House house)
    {
        human.home = house;
        house.addInhabitant(human);
    }

    private void Update()
    {
        if (WO.gameStarted)
        {
            // Define the settlement size
            settlementRadius = Mathf.Clamp((10f + houses.Count / 1.25f) / 1.5f, 0f, 15f);

            //Handles the aging
            if (aging)
            {
                timeLeft -= Time.deltaTime;

                if (timeLeft < 0)
                {
                    // Possibly spawn fish
                    foreach (var saltTile in saltWaterTiles)
                    {
                        // Spawn a fish tile ontop of a salt tile with a random chance
                        // total amount of fish is (total salt tiles / 20)
                        if (fish.Count < saltWaterTiles.Count / 20 && Random.Range(0f, 100f) > 90f)
                            saltTile.SpawnFish();
                    }

                    timeLeft = timePerYear;
                    year++;
                    //Reset the lovers
                    lovers.Clear();
                    NotifyHumans(HumanNotify.increaseAge);

                    if (currentGeneration - lastCleanedGeneration > distanceBetweenCleaning)
                    {
                        DeleteHumans();
                        lastCleanedGeneration = currentGeneration;
                    }

                    //enable snowstorm
                    if (currentGeneration == snowStormGeneration && snowStorm == false)
                    {
                        snowStorm = true;
                        if (!snowStormParticleSystem.isPlaying)
                        {
                            snowStormParticleSystem.Play();
                            snowStormOverlay.gameObject.SetActive(true);
                        }

                        Debug.Log("WARNING: A hefty snowstorm enters the land. Making the humans deplete their resources quicker, and increasing the chances of damage.");
                    }
                }
            }

            //Clamp values
            water = Mathf.Max(water, 0);
            food = Mathf.Max(food, 0);
        }
    }

    private void DeleteHumans()
    {
        for (int i = 0; i < passedAway.Count; i++)
        {
            Destroy(passedAway[i].gameObject);
        }
        passedAway.Clear();
    }


    /// <summary>
    /// Method to send a message to all humans at the same time.
    /// </summary>
    /// <param name="notification"></param>
    private void NotifyHumans(HumanNotify notification)
    {
        for (int i = 0; i < humans.Count; i++)
        {
            humans[i].Notify(notification, i);
        }

        //Check for baby making after everyone has chosen their action.
        if (notification == HumanNotify.increaseAge && humans.Count < maxAmountOfHumans)
        {
            makeBaby();
        }
    }

    /// <summary>
    /// The whole action to actually look for people that want to have a baby, combining them and creating a child
    /// </summary>
    private void makeBaby()
    {
        Human loverOne = null;
        Human loverTwo = null;

        //Go over all males and find matches for them

        for (int i = 0; i < lovers.Count; i++)
        {
            if (loverOne == null)
            {
                loverOne = lovers[i];
                Human partner = loverOne.getlifePartner();
                bool cheating = false;
                //If they have a partner, try and make love with them.
                if (partner != null)
                {
                    if (partner.currentTask == HumanAction.makeChild)
                    {
                        loverTwo = partner;
                    }
                    else
                    {
                        //Will loverOne cheat one Lover Two?
                        if (!DiceRoll(20, partner.charisma, 5 + loverOne.charisma, StatType.Strength))
                        {
                            Debug.Log(loverOne.gameObject.name + " cheated on " + partner.gameObject.name);
                            cheating = true;
                            partner.setlifePartner(null);
                            loverOne.setlifePartner(null);
                        }
                        else
                        {
                            loverTwo = partner;

                        }

                    }

                    if (cheating == false)
                    {
                        // TODO refactor to behaviour
                        Human child = makeChild(loverOne, loverTwo);
                        Debug.Log(loverOne.gameObject.name + " and " + loverTwo.gameObject.name + " just had baby number " + (loverOne.amountOfChildren + 1).ToString() + " ! Congrats on little " + child.gameObject.name);

                        child.parentOne = loverOne;
                        child.parentTwo = loverTwo;

                        child.generation = loverOne.generation + 1;
                        currentGeneration = Mathf.Max(child.generation, currentGeneration);

                        loverOne.amountOfChildren += 1;
                        loverTwo.amountOfChildren += 1;
                    }
                }
                if ((partner == null || cheating) && i + 1 < lovers.Count)
                {
                    for (int j = i + 1; j < lovers.Count; j++)
                    {
                        //If they can match
                        if (isSuitableLover(loverOne, lovers[j]))
                        {
                            loverTwo = lovers[j];

                            if (loverTwo.getlifePartner() != null)
                            {
                                if (loverTwo.getlifePartner() != loverOne)
                                {
                                    Debug.Log(loverOne.gameObject.name + " and " + loverTwo.gameObject.name + " just got together!");
                                }
                            }
                            else
                            {
                                Debug.Log(loverOne.gameObject.name + " and " + loverTwo.gameObject.name + " just got together!");
                            }
                            loverTwo.setlifePartner(loverOne);
                            loverOne.setlifePartner(loverTwo);

                            //if its a hetero couple, we can create a child
                            if (loverOne.gender != loverTwo.gender)
                            {
                                Human child = makeChild(loverOne, loverTwo);
                                Debug.Log(loverOne.gameObject.name + " and " + loverTwo.gameObject.name + " just had a baby! Congrats on little " + child.gameObject.name);

                                child.parentOne = loverOne;
                                child.parentTwo = loverTwo;

                                child.generation = loverOne.generation + 1;
                                currentGeneration = Mathf.Max(child.generation, currentGeneration);

                                loverOne.amountOfChildren += 1;
                                loverTwo.amountOfChildren += 1;
                            }
                            loverOne = null;
                            loverTwo = null;
                            //If found remove both from list, and do i--;
                            lovers.RemoveAt(j);
                            lovers.RemoveAt(i);

                        }

                    }
                }
            }


        }
    }


    /// <summary>
    /// Checks wether two humans could be lovers
    /// </summary>
    /// <param name="loverOne"></param>
    /// <param name="loverTwo"></param>
    /// <returns></returns>
    public bool isSuitableLover(Human loverOne, Human loverTwo)
    {

        //prevent person two to cheat
        if (loverTwo.getlifePartner() != null)
        {
            return false;
        }
        //Prevent incest
        if (loverOne.parentOne != null && loverTwo.parentOne != null)
        {
            if (loverTwo == loverOne.parentOne || loverTwo == loverOne.parentOne || loverOne.parentOne == loverTwo.parentOne || loverOne.parentOne == loverTwo.parentTwo || loverOne.parentTwo == loverTwo.parentTwo)
            {
                return false;
            }
        }

        //checks sexual orientation
        if (loverOne.sexualOrientation == loverTwo.sexualOrientation)
        {
            if (loverOne.sexualOrientation == Human.SexualOrientation.hetero)
            {
                if (loverOne.gender == loverTwo.gender)
                {
                    return false;
                }
            }
            if (loverOne.sexualOrientation == Human.SexualOrientation.gay)
            {
                if (loverOne.gender != loverTwo.gender)
                {
                    return false;
                }
            }
        }

        //Two different sexual orientations can only mix if one of them is bi.
        if (loverOne.sexualOrientation != loverTwo.sexualOrientation)
        {
            if (loverOne.sexualOrientation != Human.SexualOrientation.bi || loverTwo.sexualOrientation != Human.SexualOrientation.bi)
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Creating a human with the following stats
    /// </summary>
    /// <param name="strength"></param>
    /// <param name="dexterity"></param>
    /// <param name="intelligence"></param>
    /// <param name="charisma"></param>
    /// <param name="maxHealth"></param>
    /// <param name="maxFood"></param>
    /// <param name="maxWater"></param>
    /// <returns></returns>
    private Human createHuman(int strength, int dexterity, int intelligence, int charisma, int maxHealth, int maxFood, int maxWater, Human.Gender gender, int amountOfChildrenModifier, int childrenAgeModifier, int ageModifier, int age)
    {
        GameObject human = Instantiate(humanPrefab, new Vector3(0f, terrainParent.transform.position.y, 0f), Quaternion.identity); // TODO make variable
        Human newHuman = human.GetComponent<Human>();
        human.transform.position = storehouse.transform.position;

        //Instantiate the human
        newHuman.strength = strength;
        newHuman.dexterity = dexterity;
        newHuman.intelligence = intelligence;
        newHuman.charisma = charisma;
        newHuman.maxFood = maxFood;
        newHuman.maxHealth = maxHealth;
        newHuman.maxWater = maxWater;


        newHuman.amountOfChildrenModifier = amountOfChildrenModifier;
        newHuman.childrenAgeModifier = childrenAgeModifier;
        newHuman.ageModifier = ageModifier;
        newHuman.age = age;

        //Determine Gender
        if (gender == Human.Gender.random)
        {
            gender = getRandomGender();
        }

        newHuman.gender = gender;

        bool generateMaleName = (gender == Human.Gender.male);
        //Generate it's own name
        human.name = nameGen.GetNextRandomName(generateMaleName);

        newHuman.sexualOrientation = GetRandomSexualOrientation();

        humans.Add(newHuman);

        //Find a home
        for (int i = 0; i < houses.Count; i++)
        {
            if (houses[i].getInhabitantAmount() < houses[i].maxAmountOfInhabitants)
            {
                houses[i].FindHumans();
            }
        }

        return newHuman;
    }

    Human.Gender getRandomGender()
    {
        float maxRange;
        float random;
        Human.Gender gender;
        //Determine Gender
        maxRange = 2;
        random = Random.Range(0, maxRange);
        if (random > maxRange * chanceToGetMale)
        {
            gender = Human.Gender.male;
        }
        else
        {
            gender = Human.Gender.female;
        }

        return gender;
    }

    Human.SexualOrientation GetRandomSexualOrientation()
    {
        float random;
        Human.SexualOrientation orientation;
        random = Random.Range(0, chanceToGetHetero + chanceToGetGay + chanceToGetBi);
        if (random < chanceToGetHetero)
        {
            orientation = Human.SexualOrientation.hetero;
        }
        else
        {
            random = Random.Range(0, chanceToGetGay + chanceToGetBi);
            if (random > chanceToGetGay)
            {
                orientation = Human.SexualOrientation.bi;
            }
            else
            {
                orientation = Human.SexualOrientation.gay;
            }
        }

        return orientation;
    }


    /// <summary>
    /// Creating the house object. To be called when people build a house.
    /// </summary>
    /// <returns></returns>
    private House createHouse()
    {
        GameObject house = Instantiate(housePrefab.gameObject);
        var pos = BuildUtility.GetNewBuildPosition(settlementRadius, 0, 25, storehouse.gameObject, House.houseSize.x);
        house.transform.position = new Vector3(pos.x, .5f, pos.z);
        House newHouse = house.GetComponent<House>();
        houses.Add(newHouse);
        house.gameObject.name = "HouseNumber" + houses.Count.ToString();
        return newHouse;
    }

    /// <summary>
    /// Sets the stats for a new human, based on two other humans.
    /// </summary>
    /// <param name="father"></param>
    /// <param name="mother"></param>
    /// <returns>human object</returns>
    public Human makeChild(Human father, Human mother)
    {
        int mutation = Random.Range(0, geneMutation);

        Human.Gender gender = getRandomGender();
        int maleModifier = 0;
        int femaleModifer = 0;

        if (Random.Range(0, 1) < chanceOnGenderModifier)
        {
            //Mutate based on gender
            if (gender == Human.Gender.male)
            {

                maleModifier = Random.Range(0, maleMutation + 1);
            }
            else
            {
                femaleModifer = Random.Range(0, femaleMutation + 1);
            }
        }

        int strength = getChildStat(father.strength, mother.strength, mutation + maleModifier);
        int dexterity = getChildStat(father.dexterity, mother.dexterity, mutation + maleModifier);
        int intelligence = getChildStat(father.intelligence, mother.intelligence, mutation + femaleModifer);
        int charisma = getChildStat(father.charisma, mother.charisma, mutation + femaleModifer);
        int maxHealth = getChildStat(father.maxHealth, mother.maxHealth, mutation);
        int maxFood = getChildStat(father.maxFood, mother.maxFood, mutation);
        int maxWater = getChildStat(father.maxWater, mother.maxWater, mutation);
        int amountOfChildrenModifier = getChildStat(father.amountOfChildrenModifier, mother.amountOfChildrenModifier, mutation);
        int childrenAgeModifier = getChildStat(father.childrenAgeModifier, mother.childrenAgeModifier, mutation);
        int ageModifier = getChildStat(father.ageModifier, mother.ageModifier, mutation);

        Human child = createHuman(strength, dexterity, intelligence, charisma, maxHealth, maxFood, maxWater, gender, amountOfChildrenModifier, childrenAgeModifier, ageModifier, 0);

        return child;
    }

    /// <summary>
    /// Method for mixing the mothers stat with the fathers stat and including a mutation.
    /// </summary>
    /// <param name="fatherStat"></param>
    /// <param name="motherStat"></param>
    /// <param name="mutation"></param>
    /// <returns></returns>
    private int getChildStat(int fatherStat, int motherStat, int mutation)
    {
        int childStat;

        int minStat = Mathf.Min(motherStat, fatherStat) - mutation;
        int maxStat = Mathf.Max(motherStat, fatherStat) + mutation;

        childStat = Random.Range(minStat, maxStat);

        return childStat;
    }

    /// <summary>
    /// Get the amount of humans that are alive right now.
    /// </summary>
    /// <returns></returns>
    public int getAmountOfHumans()
    {
        int count = humans.Count;
        if (count == null) count = 0;
        return count;
    }

    /// <summary>
    /// Removes human from the list of alive humans
    /// </summary>
    /// <param name="index"></param>
    public void removeHuman(int index)
    {
        humans.RemoveAt(index);
    }

    /// <summary>
    /// Amount of houses
    /// </summary>
    /// <returns></returns>
    public int getAmountOfHouses()
    {
        int count = houses.Count;
        if (count == null) count = 0;
        return count;
    }

    /// <summary>
    /// List of all alive humans
    /// </summary>
    public List<Human> Humans
    {
        get { return humans; }
        set { humans = value; }
    }

    /// <summary>
    /// Returns the difference between rolled number and min number to succeed
    /// </summary>
    /// <param name="EyesOnDice"></param>
    /// <param name="modifier"></param>
    /// <param name="minNumberToSucceed"></param>
    /// <returns></returns>
    public int DiceRollDifference(int EyesOnDice, int modifier, int minNumberToSucceed)
    {
        return Mathf.Abs(Mathf.Min((Random.Range(1, EyesOnDice) + modifier), EyesOnDice) - minNumberToSucceed);
    }

    /// <summary>
    /// returns wether diceroll is succesful or not
    /// </summary>
    /// <param name="EyesOnDice"></param>
    /// <param name="modifier"></param>
    /// <param name="minNumberToSucceed"></param>
    /// <returns></returns>
    public bool DiceRoll(int EyesOnDice, int modifier, int minNumberToSucceed, StatType stat, Human targetedHuman = null)
    {
        var b = (Random.Range(1, EyesOnDice) + modifier > minNumberToSucceed);
        if (b && targetedHuman != null)
        {
            switch (stat)
            {
                default:
                case StatType.Intelligence:
                    targetedHuman.intelligence += 1;
                    break;
                case StatType.Dexterity:
                    targetedHuman.dexterity += 1;
                    break;
                case StatType.Strength:
                    targetedHuman.strength += 1;
                    break;
            }
        }
        return b;
    }

    public void SetScreenRefferences()
    {
        humanStatScreen = GameObject.FindGameObjectWithTag("StatsPanel");
        StorageStatScreen = GameObject.FindGameObjectWithTag("StorageStatsPanel");
        humanStatScreen.SetActive(false);
        StorageStatScreen.GetComponent<StorageStatsPanel>().UpdateData();
    }

    /// <summary>
    /// Sets the speed of the game clamped between two bounds 
    /// </summary>
    /// <param name="amount"></param>
    public void SetGameSpeed(float amount = 1f)
    {
        Time.timeScale = Mathf.Clamp(amount, 0f, 10f);
    }

    private void OnDrawGizmos()
    {
        if (storehouse)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(storehouse.transform.position, settlementRadius);
        }
    }
}

public enum StatType
{
    Intelligence,
    Dexterity,
    Strength,
}