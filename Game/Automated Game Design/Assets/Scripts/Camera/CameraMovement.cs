﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    [Header("Movement variables")]
    public float smoothness = 10;
    public float panSpeed = 0.35f;
    public float mousePanSpeed = 20f;
    public float panBorderThickness = 10f;
    public float minX = -10;
    public float maxX = 10;
    public float minY = -10;
    public float maxY = 10;
    [Space]
    [Header("Zoom variables")]
    public float minFov = 5;
    public float maxFov = 20;
    public float scrollSensitivity = 5;
    public float scrollSpeed = 20;

    // TEST
    public float zoomProgress;
    private float realMaxY;

    private Vector3 targetPosition;

    private void Start()
    {
        targetPosition = transform.position;
        zoomProgress = (transform.position.y / maxFov) * maxFov;
        realMaxY = maxY + (zoomProgress * maxFov);
    }

    private void Update()
    {
        MouseMovement();
        Movement();

        Camera.main.orthographicSize = transform.position.y;
    }

    private void LateUpdate()
    {
        Zoom();
        LerpZoom();
        zoomProgress = 1f - ((transform.position.y - minFov) / (maxFov - minFov));
        realMaxY = maxY + (zoomProgress * maxFov);
    }

    /// <summary>
    /// Moves the camera using the Horizontal keys (A, D, Left Arrow and Right Arrow)
    /// and the Vertical keys (W, S, Up Arrow, Down Arrow). It's being smoothed by
    /// lerping the transform position with the target position.
    /// </summary>
    private void Movement()
    {
        if (Input.GetButton("Horizontal"))
        {
            targetPosition += -Vector3.left * Input.GetAxis("Horizontal") * panSpeed;
        }
        if (Input.GetButton("Vertical"))
        {
            targetPosition += Vector3.forward * Input.GetAxis("Vertical") * panSpeed;
        }

        transform.position = Vector3.Lerp(transform.position, targetPosition, smoothness * (Time.deltaTime / Time.timeScale));
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minX, maxX), transform.position.y, Mathf.Clamp(transform.position.z, minY, realMaxY));
        targetPosition = new Vector3(Mathf.Clamp(targetPosition.x, minX, maxX), targetPosition.y, Mathf.Clamp(targetPosition.z, minY, realMaxY));
    }

    /// <summary>
    /// the movement for the camera using the mouse.
    /// if the mouse hits the edges of the screen it moves the camera that way.
    /// </summary>
	private void MouseMovement()
    {
        if (Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            targetPosition.z += mousePanSpeed * (Time.deltaTime / Time.timeScale);
        }
        if (Input.mousePosition.y <= panBorderThickness)
        {
            targetPosition.z -= mousePanSpeed * (Time.deltaTime / Time.timeScale);
        }
        if (Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            targetPosition.x += mousePanSpeed * (Time.deltaTime / Time.timeScale);
        }
        if (Input.mousePosition.x <= panBorderThickness)
        {
            targetPosition.x -= mousePanSpeed * (Time.deltaTime / Time.timeScale);
        }
    }

    /// <summary>
    /// Zooms the camera by using the scrollwheel on the mouse.
    /// </summary>
    private void Zoom()
    {
        targetPosition -= Vector3.up * Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
        targetPosition = new Vector3(transform.position.x, Mathf.Clamp(targetPosition.y, minFov, maxFov), transform.position.z);
    }

    /// <summary>
    /// Lerps the field of view of the camera. The zooming goes faster if you have a low sensitivity. 
    /// </summary>
    private void LerpZoom()
    {
        transform.position = new Vector3(transform.position.x,
            Mathf.Lerp(transform.position.y, targetPosition.y, (scrollSensitivity * (Time.deltaTime / Time.timeScale))),
            transform.position.z);
    }
}
