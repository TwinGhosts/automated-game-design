﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    public static Vector3 houseSize = Vector3.one;

    [Header("Build Variables")]
    public float buildTime = 4f;
    [HideInInspector] public bool isBeingBuilt = false;
    public bool isBuilt = true;
    public bool IsActive { get { return !isBeingBuilt && isBuilt; } }
    public int maxAmountOfInhabitants = 4;
    List<string> inhabitants;
    private void Awake()
    {
        // When the house isn't built yet, make it transparent
        if (!isBuilt)
        {
            var color = GetComponent<MeshRenderer>().materials[0].color;
            color.a = 0f;
            GetComponent<MeshRenderer>().materials[0].color = color;
        }

        //Creates a list for the inhabitants
        inhabitants = new List<string>();

    }

    // Start is called before the first frame update
    private void Start()
    {
        if (World.WO) World.WO.houses.Add(this);
        transform.SetParent(World.WO.houseParent);
    }

    /// <summary>
    /// Adds an inhabitant
    /// </summary>
    /// <param name="human">The human to add</param>    
    public void addInhabitant(Human human)
    {

        string name = human.gameObject.name;
        inhabitants.Add(name);
    }

    /// <summary>
    /// Returns the amount of inhabitants
    /// </summary>
    /// <param name="human"></param>
    public int getInhabitantAmount()
    {
        return inhabitants.Count;
    }

    /// Remove Inhabitant
    /// </summary>
    /// <param name="human">the human to remove</param>
    public void removeInhabitant(Human human)
    {
        string name = human.gameObject.name;
        inhabitants.Remove(name);

        //Find another human to take their place.
        FindHumans();

    }

    /// <summary>
    /// Checks and runs the coroutine for the visual building
    /// </summary>
    public void Build()
    {
        if (!isBeingBuilt && !isBuilt)
        {
            StartCoroutine(_Build());
        }
    }

    /// <summary>
    /// Easily fill this house with homeless people.
    /// </summary>
    public void FindHumans()
    {
        for (int i = 0; i < World.WO.getAmountOfHumans(); i++)
        {
            Human human = World.WO.Humans[i];
            if (human.home == null)
            {
                addInhabitant(human);
                human.home = this;
                if (inhabitants.Count >= maxAmountOfInhabitants)
                {
                    return;
                }
            }
        }
    }

    private IEnumerator _Build()
    {
        var progress = 0f;
        var mainColor = GetComponent<MeshRenderer>().materials[0].color;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + ((Random.Range(0f, 100f) > 50f) ? 0f : 90f), transform.rotation.eulerAngles.z);

        // Finished color, no opacity
        var endColor = mainColor;
        endColor.a = 1f;

        // Start color, opacity
        mainColor.a = 0f;
        var startColor = mainColor;

        isBuilt = false;

        // Start building
        isBeingBuilt = true;

        // Lerp the color over the build duration
        while (progress < 1f)
        {
            progress += Time.deltaTime / buildTime;

            // Lerp smoothly between the colors
            GetComponent<MeshRenderer>().material.color = Color.Lerp(startColor, endColor, Mathf.SmoothStep(0f, 1f, progress));
            yield return null;
        }

        // Finish building the building
        isBeingBuilt = false;
        isBuilt = true;
        FindHumans();
    }

    /// <summary>
    /// Removes it from the world list
    /// </summary>
    public void Remove()
    {
        if (World.WO && World.WO.houses.Contains(this)) World.WO.houses.Remove(this);
    }

    private void OnDestroy()
    {
        Remove();
    }
}
