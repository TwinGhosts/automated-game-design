﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider))]
public class Tile : MonoBehaviour
{
    public TileType type;
    public TileManager tileManager;
    private Material mat;

    public void Start ()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    public void OnMouseOver ()
    {
        if (World.WO.gameStarted == false)
        {
            GetComponent<MeshRenderer>().material = tileManager.GetActiveTileMaterial();
            GetComponent<MeshRenderer>().material.color = new Color(mat.color.r, mat.color.g, mat.color.b, 0.5f);

            if (Input.GetMouseButton(0))
            {
                if (World.WO.gameStarted == false)
                {
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        type = tileManager.GetActiveType();
                        mat = tileManager.GetActiveTileMaterial();

                        GameObject tile = Instantiate(tileManager.GetActiveGameObject(), transform.position, transform.rotation);
                        tile.transform.SetParent(GameObject.Find("Terrain").transform);
                        tile.GetComponent<Tile>().type = tileManager.GetActiveType();
                        tile.GetComponent<MeshRenderer>().material = tileManager.GetActiveTileMaterial();

                        Destroy(gameObject);
                    }
                }
            }
        }
        else GetComponent<MeshRenderer>().material = mat;
    }

    public void OnMouseExit ()
    {
        if (World.WO.gameStarted == false)
        {
            GetComponent<MeshRenderer>().material = mat;
            GetComponent<MeshRenderer>().material.color = new Color(mat.color.r, mat.color.g, mat.color.b, 1f);
        }
    }
}

public enum TileType
{
    None,
    Grass,
    Water,
    SaltWater,
    Mountain,
    Snow,
    Beach,
}
