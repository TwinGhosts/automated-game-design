﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Stats
{
    [Header("Health")]
    public int health = 100;

    [Header("D&D Stats")]
    public int strength = 5;
    public int intelligence = 5;
    public int agility = 5;
    public int charisma = 5;
}
