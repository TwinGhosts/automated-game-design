﻿public static class Stat
{
    public static readonly int maxHealth = 100;
    public static readonly int minHealth = 0;

    public static readonly int generalStatMin = 0;
    public static readonly int generalStatMax = 99;
}
